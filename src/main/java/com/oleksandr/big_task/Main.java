package com.oleksandr.big_task;

public class Main {
    public static void main(String[] args) {
        WordReader wr = new WordReader();
        String[] text = wr.readFromFile();

        Splitter splitter = new Splitter();
        String[] sentences = splitter.splitToSentences(text);
        String[] words = splitter.splitToWords(text);
        String[] wordsWithoutPunctuation = splitter.splitPunctuations(words);

        TextHandler handler = new TextHandler();
        handler.changeSentence(sentences);

        Sorter sorter = new Sorter();
        //sorter.sortByAmountOfLetterDesc(wordsWithoutPunctuation);
    }
}
