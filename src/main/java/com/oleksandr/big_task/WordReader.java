package com.oleksandr.big_task;

import java.io.*;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

public class WordReader {
    public String[] readFromFile() {
        File file;
        WordExtractor extractor;

        try {
            file = new File("C:\\Users\\sashk\\Desktop\\String.doc");
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());

            HWPFDocument doc = new HWPFDocument(fis);
            extractor = new WordExtractor(doc);

            String[] fileData = extractor.getParagraphText();
            return fileData;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
