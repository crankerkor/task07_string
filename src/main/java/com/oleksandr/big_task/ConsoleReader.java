package com.oleksandr.big_task;

import java.util.Scanner;

public class ConsoleReader {

    public int getInt (int left, int right) {
        Scanner scanner = new Scanner(System.in, "utf-8");
        int result;

        do {
            System.out.println("Enter the integer in bounds: "
                    + left + " " + right);
            while (!scanner.hasNextInt()) {
                scanner.next();
            }

            result = scanner.nextInt();
        } while (result > right || result < left);

        return result;
    }

    public char getChar() {
        Scanner scanner = new Scanner(System.in, "utf-8");
        String symbol;

        System.out.println("Enter letter: ");
        while(!scanner.hasNext("[a-zA-z]")) {
            System.out.println("Enter letter: ");
            scanner.next();
        }

        symbol = scanner.next("[a-zA-z]");

        return symbol.charAt(0);
    }

    public String getString() {
        Scanner scanner = new Scanner(System.in, "utf-8");
        String word;

        System.out.println("Enter string: ");
    while (!scanner.hasNext("[\\w]+")) {
            System.out.println("Enter string: ");
            scanner.next();
        }

        word = scanner.nextLine();
        return word;
    }
}
