package com.oleksandr.big_task;

import org.apache.poi.hssf.record.formula.functions.Char;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class TextHandler {
    private Splitter splitter;
    private ConsoleReader consoleReader;

    public TextHandler() {
        splitter = new Splitter();
        consoleReader = new ConsoleReader();
    }

    public void countMaxDuplicateWordsAmount(String[] sentences) {
        boolean hasDuplicates = false;
        int counter = 0;
        HashSet<String>  uniqueWords = new HashSet<>();

        for (String sentence : sentences) {
            String[] words = sentence.split(" ");
            words = splitter.splitPunctuations(words);
            for (String word : words) {
                if (!uniqueWords.add(word)) {
                   System.out.println(sentence + " (" + word + ")");
                   hasDuplicates = true;
                   break;
                }
            }
            if (hasDuplicates) {
               counter++;
            }
            hasDuplicates = false;
            uniqueWords = new HashSet<>();
        }

        System.out.println("Amount of sentences with duplicates: " + counter);


    }

    public void showAscending(String[] sentences) {
        Map<Integer, List<String>>  sentencesMap = new TreeMap<>();

        for (String sentence : sentences) {
            String[] words = sentence.split(" ");
            words = splitter.splitPunctuations(words);
            if (words.length == 0) {
              System.out.println(sentence);
            }
            int key = words.length;
            sentencesMap.computeIfAbsent(key, k -> new ArrayList<>()).add(sentence);
        }

        System.out.println(sentences.length + " " + sentencesMap.size());
        sentencesMap.forEach((key, value) ->
                System.out.println("words amount: " + key + " values: " + value));


    }

    public void showUniqueWordInFirstSentence(String[] sentences) {
        String firstSentence = sentences[0];
        boolean isUnique = true;
        List<String> uniqueWords = new ArrayList<>();

        String[] words = firstSentence.split(" ");
        words = splitter.splitPunctuations(words);

        for (String word : words) {
            isUnique = true;
            for (int i = 1; i < (sentences.length - 1); i++) {
                if (sentences[i].contains(word)) {
                    isUnique = false;
                    break;
                }
            }

            if (isUnique) {
               uniqueWords.add(word);
            }
        }

        System.out.println("Unique words in 1st sentence: ");
        uniqueWords.forEach(System.out::println);
    }

    public void showSameSizeWords(String[] sentences) {
        System.out.println("Enter word length: ");
        int wordLength = consoleReader.getInt(2, 20);

        List<String> questions = new ArrayList<>();
        Set <String> wordsWithoutDuplicates = new HashSet<>();

        for (String sentence : sentences) {
            if (sentence.endsWith("? ")) {
                questions.add(sentence);
            }
        }

        for (String question : questions) {
            String[] words = question.split(" ");
            words = splitter.splitPunctuations(words);
            System.out.println(question);

            for (String word : words) {
                if (word.length() == wordLength) {
                    if (wordsWithoutDuplicates.add(word)) {
                        System.out.println("word: " + word);
                    }
                }
            }
        }
    }

    public void swapWords(String[] sentences) {
        String maxLengthWord;
        Pattern pattern = Pattern.compile(" [aAuUiIeEoO]\\S+ | [AUIEO]\\S+ ");

        for (String sentence : sentences) {
            String[] words = splitter.splitPunctuations(
                    sentence.split(" "));
            maxLengthWord = getMaxLengthWord(words);

            Matcher matcher = pattern.matcher(sentence);
            if (matcher.find()) {
                sentence = swap(sentence,
                        sentence.substring(matcher.start() + 1,
                        matcher.end() - 1), maxLengthWord);

                System.out.println(sentence);
            }
        }
    }

    public void findWordsFromList(String[] sentences) {
        HashSet <String> set = new HashSet<>();
        Map <Integer, List<String>> resultingMap = new TreeMap(Collections.reverseOrder());

        System.out.println("How many words? (min: 2)");
        int listLength = consoleReader.getInt(2, 5);
        for (int i = 0; i < listLength; i++) {
            set.add(consoleReader.getString());
        }

        int frequency;

        for (String word : set) {
            Pattern pattern = Pattern.compile(word);

            frequency = 0;
            for (String sentence : sentences) {
                Matcher matcher = pattern.matcher(sentence);
                while (matcher.find()) {
                    frequency++;
                }
            }
            resultingMap.computeIfAbsent(frequency, k -> new ArrayList<>()).add(word);
        }

        resultingMap.forEach((k, v) ->
                System.out.println("num of occurrences: " + k + " word: " + v));
    }

    public void removeBiggestSequences(String[] sentences) {
        char startSym = consoleReader.getChar();
        char endSym = consoleReader.getChar();

        Pattern pattern = Pattern.compile(startSym + "[\\S\\s]*" + endSym);

        for (String sentence : sentences) {
            Matcher matcher = pattern.matcher(sentence);

            if (matcher.find()) {
                sentence = sentence.replace(
                        sentence.substring(matcher.start(), matcher.end()),
                        "");
                System.out.println(sentence);
            }
        }
    }

    public void removeConsonantStarting(String[] sentences) {
        int desiredLength = consoleReader.getInt(1, 25);

        for (String sentence : sentences) {
            String[] words = splitter.splitPunctuations(
                    sentence.split(" "));
            for (String word : words) {
                if (word.length() == desiredLength) {
                    if (startsWithConsonant(word)) {
                        sentence = sentence.replace(word, "");
                    }
                }
            }
            sentence = sentence.replaceAll("\\s{2,}", " ");
            System.out.println(sentence);
        }
    }

    public void findLargestPalindrome(String[] sentences) {
        Stream<String> stream = Stream.of(sentences);
        Optional<String> optionalResult = stream.reduce(String::concat);
        String result = null;

        if (optionalResult.isPresent()) {
            result = optionalResult.get();
        }

        int maxLen = 0;

        if (result != null) {
          int length = result.length();

          int low, high, start = 0;

          int[] array;

          for (int i = 1; i < length; i++) {
              low = i - 1;
              high = i;

              array = checkIfPalindrome(result, maxLen, low, high, start);

              start = array[0];
              maxLen = array[1];

              low = i - 1;
              high = i + 1;

              array = checkIfPalindrome(result, maxLen, low, high, start);

              start = array[0];
              maxLen = array[1];
          }

          System.out.println(result.substring(start, start + maxLen));
        }
    }

    public void changeAllWords(String[] words) {
        for (String word : words) {
            char firstLetter = word.charAt(0);
            char[] charArray = word.toCharArray();

            for (int i = 1; i < charArray.length; i++) {
                if (charArray[i] == Character.toLowerCase(firstLetter) ||
                    charArray[i] == firstLetter) {
                    charArray[i] = ' ';
                }
            }

            word = new String(charArray);
            word = word.replaceAll(" ", "");
            System.out.println(word);
        }
    }

    public void changeSentence(String[] sentences) {
        int sentenceNumber = consoleReader.getInt(0, sentences.length);
        String desiredSentence = sentences[sentenceNumber];
        System.out.println(desiredSentence);

        System.out.println("Desired length of word: ");
        int wordLen = consoleReader.getInt(1, 25);

        System.out.println("Desired string to replace: ");
        String toReplaceWith = consoleReader.getString();

        String[] words = splitter.splitPunctuations(
                desiredSentence.split(" "));
        for (String word : words) {
            if (word.length() == wordLen) {
                desiredSentence = desiredSentence.replace(word, toReplaceWith);
            }
        }

        System.out.println("Changed string: " + desiredSentence);
    }

    private int[] checkIfPalindrome(String result, int maxLen,
                                int low, int high, int start) {

        int[] resultingArray = new int[2];
        resultingArray[0] = start;
        resultingArray[1] = maxLen;

        while (low >= 0 && high < result.length() &&
                result.charAt(low) == result.charAt(high)) {
            if (high - low + 1 > maxLen) {
                maxLen = high - low + 1;
                resultingArray[0] = low;
                resultingArray[1] = maxLen;
            }
            low--;
            high++;
        }

        return resultingArray;
    }

    private boolean startsWithConsonant(String word) {
        Pattern pattern = Pattern.compile("^[b-df-hj-np-tv-z]|^[B-DF-HJ-NP-TV-Z]");
        Matcher matcher = pattern.matcher(word);

        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }

    public double getVowelPercentage(String word) {
        Pattern pattern = Pattern.compile("[aAoOuUiIeE]");
        Matcher matcher = pattern.matcher(word);

        int vowelCounter = 0;
        int length = word.length();

        while (matcher.find()) {
            vowelCounter++;
        }

        return   (double) vowelCounter / (double) length;
    }

    public String getFirstConsonant(String word) {
        Pattern pattern = Pattern.compile("[b-df-hj-np-tv-z]|[B-DF-HJ-NP-TV-Z]");
        Matcher matcher = pattern.matcher(word);

        if (matcher.find()) {
            return word.substring(matcher.start(), matcher.start() + 1);
        } else {
            return "z";
        }
    }

    public int getFrequency(String word, char symbol) {
        int frequency = 0;

        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == symbol) {
                frequency++;
            }
        }

        return frequency;
    }

    private String getMaxLengthWord(String[] words) {
        int maxLength = 0;
        String longest = "";

        for (String word : words) {
            if (word.length() > maxLength) {
                maxLength = word.length();
                longest = word;
            }
        }

        return longest;
    }

    private String swap(String sentence, String first, String second) {
        String result;
        result = sentence.replace(second,  second + "swappable");
        result = result.replace(first,   second);
        result = result.replace(second +"swappable", first);

        result = result.replaceAll("\\s", " ");

        return result;
    }


}
