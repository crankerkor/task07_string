package com.oleksandr.big_task;

import org.apache.poi.hssf.record.formula.functions.Match;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sorter {
    private TextHandler handler;
    private ConsoleReader reader;

    public Sorter() {
        reader = new ConsoleReader();
        handler = new TextHandler();
    }

    public void sortByFirstLetter(String[] words) {
        Set <String> wordsWithoutDuplicates = new HashSet<>();
        Arrays.sort(words, (a, b) ->
               a.substring(0, 1).compareToIgnoreCase(b.substring(0,1)) );

        String firstLetter = words[0].substring(0, 1);
        for (String word : words) {
            if(wordsWithoutDuplicates.add(word)) {
                if (!word.substring(0, 1).equalsIgnoreCase(firstLetter)) {
                    System.out.println();
                    firstLetter = word.substring(0, 1);
                }
                    System.out.println(word);
            }
        }
    }

    public void sortByVowelPercentage(String[] words) {
        Set <String> wordsWithoutDuplicates = new HashSet<>();

        Arrays.sort(words, Comparator.comparingDouble(handler::getVowelPercentage));

        for (String word : words) {
            if (wordsWithoutDuplicates.add(word)) {
                System.out.println(word +
                        " vowel percentage: " + handler.getVowelPercentage(word));
            }
        }

    }

    public void sortVowelStartingByFirstConsonant(String[] words) {
        Pattern pattern = Pattern.compile("^[aAuUiIeEoO]\\S+|^[AUIEO]\\S+");

        Set <String> wordsWithoutDuplicates = new HashSet<>();

        for (String word : words) {
            Matcher matcher = pattern.matcher(word);
            if (matcher.find()) {
                wordsWithoutDuplicates.add(word);
            }
        }

        String[] wordsWithoutDuplicatesArray =
                new String[wordsWithoutDuplicates.size()];

        wordsWithoutDuplicates.toArray(wordsWithoutDuplicatesArray);

        Arrays.sort(wordsWithoutDuplicatesArray, (a, b) ->
                handler.getFirstConsonant(a).
                        compareToIgnoreCase
                                (handler.getFirstConsonant(b)));

        for (String word : wordsWithoutDuplicatesArray) {
            System.out.println(word);
        }


    }

    public void sortByAmountOfLetter(String[] words) {
        char sym = reader.getChar();

        words = deleteDuplicates(words);

        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < words.length - 1; j++) {
                if (handler.getFrequency(words[j], sym) >
                    handler.getFrequency(words[j + 1], sym)) {
                    String swapper = words[j];
                    words[j] = words[j + 1];
                    words[j + 1] = swapper;
                }
            }
        }

        sortByAlphabet(words, sym);

        for (String word : words) {
            System.out.println(word + " " + handler.getFrequency(word, sym));
        }
    }

    public void sortByAmountOfLetterDesc(String[] words) {
        char sym = reader.getChar();

        words = deleteDuplicates(words);

        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < words.length - 1; j++) {
                if (handler.getFrequency(words[j], sym) <
                        handler.getFrequency(words[j + 1], sym)) {
                    String swapper = words[j];
                    words[j] = words[j + 1];
                    words[j + 1] = swapper;
                }
            }
        }

        sortByAlphabet(words, sym);

        for (String word : words) {
            System.out.println(word + " " + handler.getFrequency(word, sym));
        }
    }

    private void sortByAlphabet(String[] words, char sym) {
        Arrays.sort(words, (a, b) -> {
            if (handler.getFrequency(a, sym) == handler.getFrequency(b, sym)) {
                return a.compareToIgnoreCase(b);
            } else {
                return 0;
            }
        });
    }

    private String[] deleteDuplicates(String[] strings) {
        Set <String> wordsWithoutDuplicates = new HashSet<>();

        for (String str : strings) {
            wordsWithoutDuplicates.add(str);
        }

        String[] wordsWithoutDuplicatesArray =
                new String[wordsWithoutDuplicates.size()];

        return wordsWithoutDuplicates.toArray(wordsWithoutDuplicatesArray);
    }

}
