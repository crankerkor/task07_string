package com.oleksandr.big_task;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.*;

public class Splitter {

    public String[] splitToWords (String[] paragraphs) {
        List <String[]> sentences = new ArrayList<>();
        for (String paragraph : paragraphs) {
            String[] words = paragraph.split("\\s");
            sentences.add(words);
        }

        String[][] matrix = sentences.toArray(new String[0][0]);

        return castMatrixToArray(matrix);
    }

    public String[] splitToSentences(String[] paragraphs) {
        List<String> sentences = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        String sentence;

        for (String paragraph : paragraphs) {
            iterator.setText(paragraph);
            int start = iterator.first();

            for (int end = iterator.next();
                end != BreakIterator.DONE;
                start = end, end = iterator.next()) {

                sentence = (paragraph.substring(start, end));
                sentence = sentence.replaceAll("\\s", " ");
                if (!sentence.equals("  ")) {
                    sentence = sentence.replaceAll("  ", " ");
                    sentences.add(sentence);
                }

            }
        }

        return sentences.toArray(new String[0]);
    }

    public String[] splitPunctuations(String[] wordsWithPunctuation) {
        List<String> words = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\w+-?:?:?\\w*");

        for (String wordWithPunctuation : wordsWithPunctuation) {
            Matcher matcher = pattern.matcher(wordWithPunctuation);

            while (matcher.find()) {
                words.add(wordWithPunctuation.substring(matcher.start(), matcher.end()));
            }
        }

        return words.toArray(new String[0]);
    }

    private String[] castMatrixToArray(String[][] sentences) {
        int sum = 0, counter = 0;

        for (String[] array : sentences) {
            sum += array.length;
        }

        String[] words = new String[sum];

        for (int i = 0; i < sentences.length; i++) {
            for (int j = 0; j < sentences[i].length; j++) {
                words[counter] = sentences[i][j];
                counter++;
            }
        }

        return words;
    }
}
